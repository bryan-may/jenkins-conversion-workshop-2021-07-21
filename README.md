# Jenkins Conversion Workshop 2021-07-21

Check out the [user guide for the Jenkins Conversion Workshop](UserGuide-JenkinsConversionWorkshop.pdf) on 2021-07-2021. 

Once you've navigated to the pdf, download it using the down arrow:

![](download.png)
